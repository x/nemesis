===============================
Nemesis
===============================

Nemesis provides a pluggable file intelligence API for the OpenStack ecosystem. 

* Free software: Apache license
* Source: http://git.openstack.org/cgit/openstack/nemesis
* Bugs: http://bugs.launchpad.net/python-nemesis

