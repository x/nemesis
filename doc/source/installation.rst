============
Installation
============

Nemesis is a very early development project and hence deploying it to a production cloud is not advised (yet), however it's requirements are quite
minimalistic and hence setting up a small dev / test environment is pretty easy.


Prerequisites
-------------

So far only installation on Ubuntu is described however installation should be very similar and straight forward on other distributions. For a 
minimalist installation of Nemesis you'll require:

* 2 VMs / Physical hosts (1 for the API node, and 1 for a worker node)
* OpenStack Keystone (for user authentication)
* OpenStack Swift (for artifact storage)
* RabbitMQ Server
* MySQL Server


API Nodes
---------


Worker Nodes
------------


Validating Installation
-----------------------
