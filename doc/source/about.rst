===============
About Nemesis
===============

Nemesis is a project which aims to add a plug-able file analysis API to an OpenStack cloud. Some use cases that come to mind include:

* Malware Analysis as a Service
* Upload analysis / tagging
* File feature extraction
